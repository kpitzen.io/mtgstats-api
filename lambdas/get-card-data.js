const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});

const regex = /[\W]/i

exports.handler = (event, context, callback) => {
  const dynamo = new AWS.DynamoDB();
  const requestedCard = event.queryStringParameters.name.toLowerCase().replace(regex, '');
  var params = {
    ExpressionAttributeNames: {
      "#N": "name"
    },
    ExpressionAttributeValues: {
     ":v1": {
       S: requestedCard
      }
    }, 
    KeyConditionExpression: "#N = :v1",
    TableName: `${process.env.REGION}-card-data`
   };
  const response = dynamo.query(params, (err, data) => {
    if (err) {
      callback(err);
    } else {
      const apiResponse = {
        statusCode: 200,
        body: JSON.stringify(data)
      }
      callback(null, apiResponse)
    }
  });
};
