variable "region" {
  default = "dev"
}

variable "subdomain" {
  default = "api"
}
