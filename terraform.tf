provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "kpitzen-ci"
    key    = "mtgstats.api.tf"
    region = "us-east-1"
  }
}

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "lambda_policy" {
  statement {
    sid = "1"

    actions = [
      "dynamodb:GetItem",
      "dynamodb:BatchGetItems",
      "dynamodb:Query",
      "logs:*",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_api_gateway_rest_api" "api" {
  name        = "${var.region}MtgStatsApi"
  description = "API for handling data transactions for MTG Stats"
}

resource "aws_api_gateway_deployment" "deployment" {
  depends_on = ["aws_api_gateway_integration.integration"]

  rest_api_id       = "${aws_api_gateway_rest_api.api.id}"
  stage_name        = "v1"
  stage_description = "Deployed at ${timestamp()}"
}

resource "aws_acm_certificate" "cert" {
  domain_name       = "${var.subdomain}.mtgstats.net"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_domain_name" "domain" {
  domain_name     = "${var.subdomain}.mtgstats.net"
  certificate_arn = "${aws_acm_certificate.cert.arn}"
}

resource "aws_api_gateway_base_path_mapping" "test" {
  api_id      = "${aws_api_gateway_rest_api.api.id}"
  domain_name = "${aws_api_gateway_domain_name.domain.domain_name}"
}

output "certificate_dns_validation" {
  value = "${aws_acm_certificate.cert.domain_validation_options}"
}
